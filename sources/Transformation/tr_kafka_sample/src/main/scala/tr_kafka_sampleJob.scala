package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class tr_kafka_sampleJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_kafka_sample"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val Kafka_0 = getKafka_0(spark)


    	val Projection_3 = getProjection_3(spark, Kafka_0)


    	Local_1(spark, Projection_3)
 
    
  }
  def getKafka_0(spark: SparkSession): Dataset[Kafka_0Schema] = {
    import spark.implicits._
    import scala.reflect.runtime.universe._
    import org.apache.avro.Schema
    import org.apache.avro.io.{DecoderFactory, EncoderFactory}
    import org.apache.avro.file.{DataFileReader, SeekableByteArrayInput}
    import org.apache.avro.generic.{GenericData, GenericDatumReader, GenericDatumWriter, GenericRecord}

 
    spark
    .read    
    .format("kafka")
    .option("kafka.bootstrap.servers", s"""kafka:9092""")
    .option("subscribe", s"""events""")
    .load()
    .as[Kafka_0Schema]
  }

  def getProjection_3(spark: SparkSession, Kafka_0: Dataset[Kafka_0Schema]) = {
    import spark.implicits._
    
    Kafka_0
    .select(
    			col("key").alias("key"),  		
     
    			expr(s"""cast(value as string)""").alias("value"),  		
     
    			expr(s"""get_json_object(cast(value as string), "$$.name")""").alias("name")		
    ).as[Projection_3Schema]
  }

  def Local_1(spark: SparkSession, ds: Dataset[Projection_3Schema]): Unit = {        
    val fileName = s"""/tmp/kafka_events.json"""
    logger.logInfo(s"LocalTarget Local_1 fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("json")
        .save(fileName)    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Kafka_0Schema(
    var key: java.lang.String,
    var value: java.lang.String
) extends Serializable
case class Projection_3Schema(
    var key: java.lang.String,
    var value: java.lang.String,
    var name: java.lang.String
) extends Serializable

}


object tr_kafka_sampleJob {
   def main(args: Array[String]): Unit = {
     new tr_kafka_sampleJob().sparkMain(args)
  }
}

