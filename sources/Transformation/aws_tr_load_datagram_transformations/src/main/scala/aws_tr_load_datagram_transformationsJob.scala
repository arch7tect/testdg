package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class aws_tr_load_datagram_transformationsJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "aws_tr_load_datagram_transformations"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val SQL_7 = getSQL_7(spark)


    	val SQL_0 = getSQL_0(spark)


    	val SQL_1 = getSQL_1(spark)


    	val Join_3 = getJoin_3(spark, SQL_0, SQL_1)


    	val Join_6 = getJoin_6(spark, SQL_7, Join_3)


    	Local_9(spark, Join_6)
 
    
  }
    
  def getSQL_0(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	auditinfo_auditinfo_e_id,
                      	buildversion,
                      	description,
                      	dtype,
                      	e_id,
                      	jsonview,
                      	label,
                      	name,
                      	project_project_e_id,
                      	sparkversion
                      from etl_transformation"""
    
    logger.logInfo(s"SQLSource SQL_0 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }
    
  def getSQL_1(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	changedatetime,
                      	changeuser,
                      	createdatetime,
                      	createuser,
                      	dtype,
                      	e_container,
                      	econtainer_class,
                      	e_container_feature_name,
                      	e_id
                      from auth_auditinfo"""
    
    logger.logInfo(s"SQLSource SQL_1 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }
    
  def getSQL_7(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	connectasloggedinuser,
                      	dtype,
                      	e_id,
                      	name,
                      	project_parentproject_e_id,
                      	svncommitmessage,
                      	svnenabled,
                      	svnpassword,
                      	svnurl,
                      	svnusername,
                      	vcstype
                      from etl_project"""
    
    logger.logInfo(s"SQLSource SQL_7 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }

  def getExpression_11(spark: SparkSession): Dataset[Expression_11Schema] = {
  	import spark.implicits._
    val expr = {
        Array(
            Map("id" -> new java.math.BigDecimal(1), "name" -> "1"),
            Map("id" -> new java.math.BigDecimal(2), "name" -> "2") 
        )
    }
    spark.createDataset(expr.map(m => Expression_11Schema(
        id = m.getOrElse("id", null).asInstanceOf[java.math.BigDecimal],        
        name = m.getOrElse("name", null).asInstanceOf[java.lang.String]        
    )))
  }

  def getJoin_3(spark: SparkSession, SQL_0: Dataset[org.apache.spark.sql.Row], SQL_1: Dataset[org.apache.spark.sql.Row]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    SQL_0.joinWith(SQL_1, 
        SQL_0("auditinfo_auditinfo_e_id") === SQL_1("e_id"), 
        "inner")
    .select(
    		col("_1.buildversion")		.alias("buildversion"),  		
    		col("_1.description")		.alias("description"),  		
    		col("_1.e_id")		.alias("transformation_id"),  		
    		col("_1.label")		.alias("label"),  		
    		col("_1.name")		.alias("transformation_name"),  		
    		col("_1.project_project_e_id")		.alias("project_project_e_id"),  		
    		col("_1.sparkversion")		.alias("sparkversion"),  		
    		col("_2.changedatetime")		.alias("changedatetime"),  		
    		col("_2.changeuser")		.alias("changeuser"),  		
    		col("_2.createdatetime")		.alias("createdatetime"),  		
    		col("_2.createuser")		.alias("createuser")		
    )    
    .as[Join_3Schema]
  }

  def getJoin_6(spark: SparkSession, SQL_7: Dataset[org.apache.spark.sql.Row], Join_3: Dataset[Join_3Schema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    Join_3.joinWith(SQL_7, 
        Join_3("project_project_e_id") === SQL_7("e_id"), 
        "left_outer")
    .select(
    		col("_1.transformation_name")		.alias("transformation_name"),  		
    		col("_2.connectasloggedinuser")		.alias("connectasloggedinuser"),  		
    		col("_2.svncommitmessage")		.alias("svncommitmessage"),  		
    		col("_2.svnenabled")		.alias("svnenabled"),  		
    		col("_2.svnpassword")		.alias("svnpassword"),  		
    		col("_2.svnurl")		.alias("svnurl"),  		
    		col("_2.svnusername")		.alias("svnusername"),  		
    		col("_2.vcstype")		.alias("vcstype"),  		
    		col("_1.buildversion")		.alias("buildversion"),  		
    		col("_1.description")		.alias("description"),  		
    		col("_1.transformation_id")		.alias("transformation_id"),  		
    		col("_1.label")		.alias("label"),  		
    		col("_1.project_project_e_id")		.alias("project_project_e_id"),  		
    		col("_1.sparkversion")		.alias("sparkversion"),  		
    		col("_1.changedatetime")		.alias("changedatetime"),  		
    		col("_1.changeuser")		.alias("changeuser"),  		
    		col("_1.createdatetime")		.alias("createdatetime"),  		
    		col("_1.createuser")		.alias("createuser"),  		
    		col("_2.name")		.alias("project_name")		
    )    
    .as[Join_6Schema]
  }

  def Local_9(spark: SparkSession, ds: Dataset[Join_6Schema]): Unit = {        
    val fileName = s"""/tmp/transformations"""
    logger.logInfo(s"LocalTarget Local_9 fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("json")
        .save(fileName)    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Expression_11Schema(
    var id: java.math.BigDecimal,
    var name: java.lang.String
) extends Serializable
case class Join_3Schema(
    var buildversion: java.lang.String,
    var description: java.lang.String,
    var transformation_id: java.lang.Long,
    var label: java.lang.String,
    var transformation_name: java.lang.String,
    var project_project_e_id: java.lang.Long,
    var sparkversion: java.lang.String,
    var changedatetime: java.sql.Timestamp,
    var changeuser: java.lang.String,
    var createdatetime: java.sql.Timestamp,
    var createuser: java.lang.String
) extends Serializable
case class Join_6Schema(
    var transformation_name: java.lang.String,
    var connectasloggedinuser: java.lang.Boolean,
    var svncommitmessage: java.lang.String,
    var svnenabled: java.lang.Boolean,
    var svnpassword: java.lang.String,
    var svnurl: java.lang.String,
    var svnusername: java.lang.String,
    var vcstype: java.lang.String,
    var buildversion: java.lang.String,
    var description: java.lang.String,
    var transformation_id: java.lang.Long,
    var label: java.lang.String,
    var project_project_e_id: java.lang.Long,
    var sparkversion: java.lang.String,
    var changedatetime: java.sql.Timestamp,
    var changeuser: java.lang.String,
    var createdatetime: java.sql.Timestamp,
    var createuser: java.lang.String,
    var project_name: java.lang.String
) extends Serializable

}


object aws_tr_load_datagram_transformationsJob {
   def main(args: Array[String]): Unit = {
     new aws_tr_load_datagram_transformationsJob().sparkMain(args)
  }
}

