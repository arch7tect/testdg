package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext



class tr_1c_crmcomtact_list_loadJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_1c_crmcomtact_list_load"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val json_file = getjson_file(spark)


    	contact_list_orc(spark, json_file)
 
    
  }
  def getjson_file(spark: SparkSession) = {
  	import spark.implicits._
    import scala.reflect.runtime.universe._
    val fileName = s"""file:///data/crm/${jobParameters("date")}/contact.list.json"""
    val paths = fileName.split(",")
    
    spark
    .read 
    .format("json")
    .load(paths: _*) 
  }

  def contact_list_orc(spark: SparkSession, ds: Dataset[org.apache.spark.sql.Row]): Unit = {        
    val fileName = s"""/contact_list"""
    logger.logInfo(s"LocalTarget contact_list_orc fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Append)
        .format("orc")
        .save(fileName)    
    
    var partitionFields = Array[String]()
    val fieldComments = Map[String, String]((s"""ADDRESS""" ->  null), (s"""ADDRESS_2""" ->  null), (s"""ADDRESS_CITY""" ->  null), (s"""ADDRESS_COUNTRY""" ->  null), (s"""ADDRESS_LOC_ADDR_ID""" ->  null), (s"""ADDRESS_POSTAL_CODE""" ->  null), (s"""ADDRESS_PROVINCE""" ->  null), (s"""ADDRESS_REGION""" ->  null), (s"""ASSIGNED_BY_ID""" ->  null), (s"""BIRTHDATE""" ->  null), (s"""COMMENTS""" ->  null), (s"""COMPANY_ID""" ->  null), (s"""CREATED_BY_ID""" ->  null), (s"""DATE_CREATE""" ->  null), (s"""DATE_MODIFY""" ->  null), (s"""EXPORT""" ->  null), (s"""FACE_ID""" ->  null), (s"""HAS_EMAIL""" ->  null), (s"""HAS_IMOL""" ->  null), (s"""HAS_PHONE""" ->  null), (s"""HONORIFIC""" ->  null), (s"""ID""" ->  null), (s"""LAST_NAME""" ->  null), (s"""LEAD_ID""" ->  null), (s"""MODIFY_BY_ID""" ->  null), (s"""NAME""" ->  null), (s"""OPENED""" ->  null), (s"""ORIGINATOR_ID""" ->  null), (s"""ORIGIN_ID""" ->  null), (s"""ORIGIN_VERSION""" ->  null), (s"""PHOTO""" ->  null), (s"""POST""" ->  null), (s"""SECOND_NAME""" ->  null), (s"""SOURCE_DESCRIPTION""" ->  null), (s"""SOURCE_ID""" ->  null), (s"""TYPE_ID""" ->  null), (s"""UTM_CAMPAIGN""" ->  null), (s"""UTM_CONTENT""" ->  null), (s"""UTM_MEDIUM""" ->  null), (s"""UTM_SOURCE""" ->  null), (s"""UTM_TERM""" ->  null))
    def makeTypeDescription(dataType: org.apache.spark.sql.types.DataType): String = {
      if (dataType.isInstanceOf[org.apache.spark.sql.types.ArrayType]) {
        "ARRAY<" + makeTypeDescription(dataType.asInstanceOf[org.apache.spark.sql.types.ArrayType].elementType) + ">"
      }
      else if (dataType.isInstanceOf[org.apache.spark.sql.types.StructType]) {
        "STRUCT<" + dataType.asInstanceOf[org.apache.spark.sql.types.StructType].toList.map(f=>f.name + ": " + makeTypeDescription(f.dataType)).mkString(", ") + ">"
      }
      else {
        dataType.typeName
      }
    }
    var fieldStr = dsOut.schema.fields.filterNot(f => partitionFields.contains(f.name.toLowerCase)).map((f)=>{f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")}).mkString(", ")    
    var partStr = dsOut.schema.fields.filter(f => partitionFields.contains(f.name.toLowerCase)).map(f => f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")).mkString(", ")
    spark.sql(s"""DROP TABLE IF EXISTS contact_list""")
    val createQuery = s"""CREATE EXTERNAL TABLE contact_list(${fieldStr})
    ${if(partStr == null || partStr == "") "" else s"PARTITIONED BY (${partStr})"}
    STORED AS orc
    LOCATION '/contact_list'"""
    logger.logInfo(s"Create external table:\n${createQuery}")   
    spark.sql(createQuery)
 
  }


    override def initBuilder(builder: SparkSession.Builder): SparkSession.Builder = {
        builder.enableHiveSupport()
    }
     

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)

}


object tr_1c_crmcomtact_list_loadJob {
   def main(args: Array[String]): Unit = {
     new tr_1c_crmcomtact_list_loadJob().sparkMain(args)
  }
}

