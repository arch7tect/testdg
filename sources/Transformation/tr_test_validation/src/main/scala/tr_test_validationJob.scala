package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class tr_test_validationJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_test_validation"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val Expression_0 = getExpression_0(spark)


    	val Projection_1 = getProjection_1(spark, Expression_0)


    	val Spark_SQL_8 = getSpark_SQL_8(spark, Projection_1)


    	Local_3(spark, Spark_SQL_8)

    	Table_7(spark, Spark_SQL_8)
 
    
  }

  def getExpression_0(spark: SparkSession): Dataset[Expression_0Schema] = {
  	import spark.implicits._
    val expr = {
        Array(
    Map("id" -> new java.math.BigDecimal(1), "name" -> "1"),
    Map("id" -> new java.math.BigDecimal(2), "name" -> "2") 
)
    }
    spark.createDataset(expr.map(m => Expression_0Schema(
        id = m.getOrElse("id", null).asInstanceOf[java.math.BigDecimal],        
        name = m.getOrElse("name", null).asInstanceOf[java.lang.String]        
    )))
  }

  def getProjection_1(spark: SparkSession, Expression_0: Dataset[Expression_0Schema]) = {
    import spark.implicits._
    
    Expression_0
    .select(
    			col("id").alias("id"),  		
    			col("name").alias("name"),  		
     
    			expr(s"""uuid()""").alias("ext_id")		
    ).as[Projection_1Schema]
  }

  def getSpark_SQL_8(spark: SparkSession, Projection_1: Dataset[Projection_1Schema]) = {
    import spark.implicits._
    	
    		Projection_1.createOrReplaceTempView("in")
    	
    			
    	val sqlText = s"""select * 
from in
where id > 0"""	
    	val queryResult = spark.sql(s"${sqlText}")
    	
    		
    	queryResult.as[Spark_SQL_8Schema]      
  }

  def Local_3(spark: SparkSession, ds: Dataset[Spark_SQL_8Schema]): Unit = {        
    val fileName = s"""/temp/test.json"""
    logger.logInfo(s"LocalTarget Local_3 fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("json")
        .save(fileName)    
    
 
  }
  
  def Table_7(spark: SparkSession, ds: Dataset[Spark_SQL_8Schema]): Unit = {
  
    import spark.implicits._
    import scala.util.parsing.json.JSONObject
    
    val master: String = _master
    val jobStartTime: Long = spark.sparkContext.startTime
    val context = getContext("dwh").asInstanceOf[JdbcETLContext]
    
    notifyStart("Table_7", "Spark_SQL_8", jobStartTime)
    
    
    
      ds.mapPartitions(partition => {
      val rejects = new java.util.ArrayList[(String, String)]()
      val cn = context.getConnection
      
      try {
          cn.setAutoCommit(false)
          val sqlText = "insert into " + context._schema + s"""."test_uid"("ext_id", "id", "name") 
          values( ?,  ?,  ?)"""
 
          
          logger.logInfo(s"TableTarget Table_7 query: ${sqlText}")
          
          val stmt = cn.prepareCall(sqlText)
          
          try {
            partition.sliding(_slideSize, _slideSize).foreach(slide => {
              processSlice(cn, stmt, slide.toList, (row: Spark_SQL_8Schema, stmt: CallableStatement) =>{
                stmt.setObject(1, java.util.UUID.fromString(row.ext_id.asInstanceOf[java.lang.String]))
                stmt.setBigDecimal(2, row.id.asInstanceOf[java.math.BigDecimal])
                stmt.setString(3, row.name.asInstanceOf[java.lang.String])
              },(successRowCount:Int) =>{
                notifyExecUpdate("Table_7", "Spark_SQL_8", jobStartTime, successRowCount)
              },(errorMessage: String, failedRow: Spark_SQL_8Schema) =>{
                val rowMap = failedRow.getClass.getDeclaredFields.map( _.getName ).zip( failedRow.productIterator.to.map(v=>v match {case null=> "<null>" case _=> v}) ).toMap
                val rejectJson = JSONObject(rowMap).toString()
                if (rejects.size >= _rejectSize) {
                  notifyException("Table_7", "Spark_SQL_8", jobStartTime, errorMessage, rejectJson)
                  throw new RuntimeException(s"Max Rejects Limit (${_rejectSize}) Was Reached for Table_7" + errorMessage)
                }
                notifyException("Table_7", "Spark_SQL_8", jobStartTime, errorMessage, rejectJson)
                rejects.add((rejectJson, errorMessage))
              })
            })
          } finally {
            stmt.close()
          }
      } finally {
          cn.close()
      }
      JavaConversions.asScalaBuffer(rejects).toIterator
    })
    .map { reject =>{
            (_workflowId, _applicationId, getApplicationName, "Table_7", reject._1, reject._2, System.currentTimeMillis)
        }
    }
    .toDF("workflowid", "appid", "classname", "methodname", "object", "exception", "ts")
    .coalesce(1).write.mode(SaveMode.Append).format("json").save(s"${_applicationHome}/rejects.json")

    notifyFinish("Table_7", "Spark_SQL_8", jobStartTime)
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Expression_0Schema(
    var id: java.math.BigDecimal,
    var name: java.lang.String
) extends Serializable
case class Projection_1Schema(
    var id: java.math.BigDecimal,
    var name: java.lang.String,
    var ext_id: java.lang.String
) extends Serializable
case class Spark_SQL_8Schema(
    var id: java.math.BigDecimal,
    var name: java.lang.String,
    var ext_id: java.lang.String
) extends Serializable

}


object tr_test_validationJob {
   def main(args: Array[String]): Unit = {
     new tr_test_validationJob().sparkMain(args)
  }
}

