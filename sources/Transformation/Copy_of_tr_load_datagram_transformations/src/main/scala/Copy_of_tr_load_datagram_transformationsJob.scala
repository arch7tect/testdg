package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class Copy_of_tr_load_datagram_transformationsJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "Copy_of_tr_load_datagram_transformations"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val SQL_7 = getSQL_7(spark)


    	val SQL_0 = getSQL_0(spark)


    	val SQL_1 = getSQL_1(spark)


    	val Union_7 = getUnion_7(spark, SQL_0, SQL_1)


    	val Join_6 = getJoin_6(spark, SQL_7, Union_7)


    	Local_9(spark, Join_6)
 
    
  }
    
  def getSQL_0(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	auditinfo_auditinfo_e_id,
                      	buildversion,
                      	description,
                      	dtype,
                      	e_id,
                      	jsonview,
                      	label,
                      	name,
                      	project_project_e_id,
                      	sparkversion
                      from etl_transformation"""
    
    logger.logInfo(s"SQLSource SQL_0 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }
    
  def getSQL_1(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	changedatetime,
                      	changeuser,
                      	createdatetime,
                      	createuser,
                      	dtype,
                      	e_container,
                      	econtainer_class,
                      	e_container_feature_name,
                      	e_id
                      from auth_auditinfo"""
    
    logger.logInfo(s"SQLSource SQL_1 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }
    
  def getSQL_7(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	connectasloggedinuser,
                      	dtype,
                      	e_id,
                      	name,
                      	project_parentproject_e_id,
                      	svncommitmessage,
                      	svnenabled,
                      	svnpassword,
                      	svnurl,
                      	svnusername,
                      	vcstype
                      from etl_project"""
    
    logger.logInfo(s"SQLSource SQL_7 query: ${sqlText}")    
    val contextName = "teneo"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()  }

  def getJoin_6(spark: SparkSession, SQL_7: Dataset[org.apache.spark.sql.Row], Union_7: Dataset[Union_7Schema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    Union_7.joinWith(SQL_7, 
        Union_7("project_project_e_id") === SQL_7("e_id"), 
        "left_outer")
    .select(
    		col("_2.connectasloggedinuser")		.alias("connectasloggedinuser"),  		
    		col("_2.svncommitmessage")		.alias("svncommitmessage"),  		
    		col("_2.svnenabled")		.alias("svnenabled"),  		
    		col("_2.svnpassword")		.alias("svnpassword"),  		
    		col("_2.svnurl")		.alias("svnurl"),  		
    		col("_2.svnusername")		.alias("svnusername"),  		
    		col("_2.vcstype")		.alias("vcstype"),  		
    		col("_2.name")		.alias("project_name"),  		
    		col("_1.dtype")		.alias("dtype"),  		
    		col("_1.e_id")		.alias("e_id"),  		
    		col("_1.jsonview")		.alias("jsonview"),  		
    		col("_1.label")		.alias("label"),  		
    		col("_1.name")		.alias("name"),  		
    		col("_1.project_project_e_id")		.alias("project_project_e_id"),  		
    		col("_1.sparkversion")		.alias("sparkversion"),  		
    		col("_1.changedatetime")		.alias("changedatetime"),  		
    		col("_1.changeuser")		.alias("changeuser"),  		
    		col("_1.createdatetime")		.alias("createdatetime"),  		
    		col("_1.createuser")		.alias("createuser"),  		
    		col("_1.e_container")		.alias("e_container"),  		
    		col("_1.econtainer_class")		.alias("econtainer_class"),  		
    		col("_1.e_container_feature_name")		.alias("e_container_feature_name")		
    )    
    .as[Join_6Schema]
  }

  def getUnion_7(spark: SparkSession, SQL_0: Dataset[org.apache.spark.sql.Row], SQL_1: Dataset[org.apache.spark.sql.Row]) = {
    import spark.implicits._
    SQL_0
    .select(
    	col("dtype").alias("dtype")
    , 	col("e_id").alias("e_id")
    , 	col("jsonview").alias("jsonview")
    , 	col("label").alias("label")
    , 	col("name").alias("name")
    , 	col("project_project_e_id").alias("project_project_e_id")
    , 	col("sparkversion").alias("sparkversion")
    , 	expr("null").alias("changedatetime")
    , 	expr("null").alias("changeuser")
    , 	expr("null").alias("createdatetime")
    , 	expr("null").alias("createuser")
    , 	expr("null").alias("e_container")
    , 	expr("null").alias("econtainer_class")
    , 	expr("null").alias("e_container_feature_name")
    ).union(SQL_1.asInstanceOf[Dataset[org.apache.spark.sql.Row]]).as[Union_7Schema]
  }

  def Local_9(spark: SparkSession, ds: Dataset[Join_6Schema]): Unit = {        
    val fileName = s"""/tmp/transformations"""
    logger.logInfo(s"LocalTarget Local_9 fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("json")
        .save(fileName)    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Join_6Schema(
    var connectasloggedinuser: java.lang.Boolean,
    var svncommitmessage: java.lang.String,
    var svnenabled: java.lang.Boolean,
    var svnpassword: java.lang.String,
    var svnurl: java.lang.String,
    var svnusername: java.lang.String,
    var vcstype: java.lang.String,
    var project_name: java.lang.String,
    var dtype: java.lang.String,
    var e_id: java.lang.Long,
    var jsonview: java.lang.String,
    var label: java.lang.String,
    var name: java.lang.String,
    var project_project_e_id: java.lang.Long,
    var sparkversion: java.lang.String,
    var changedatetime: java.sql.Timestamp,
    var changeuser: java.lang.String,
    var createdatetime: java.sql.Timestamp,
    var createuser: java.lang.String,
    var e_container: java.lang.String,
    var econtainer_class: java.lang.String,
    var e_container_feature_name: java.lang.String
) extends Serializable
case class Union_7Schema(
    var dtype: java.lang.String,
    var e_id: java.lang.Long,
    var jsonview: java.lang.String,
    var label: java.lang.String,
    var name: java.lang.String,
    var project_project_e_id: java.lang.Long,
    var sparkversion: java.lang.String,
    var changedatetime: java.sql.Timestamp,
    var changeuser: java.lang.String,
    var createdatetime: java.sql.Timestamp,
    var createuser: java.lang.String,
    var e_container: java.lang.String,
    var econtainer_class: java.lang.String,
    var e_container_feature_name: java.lang.String
) extends Serializable

}


object Copy_of_tr_load_datagram_transformationsJob {
   def main(args: Array[String]): Unit = {
     new Copy_of_tr_load_datagram_transformationsJob().sparkMain(args)
  }
}

