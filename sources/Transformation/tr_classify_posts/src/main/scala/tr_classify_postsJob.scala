package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.commons.io.IOUtils
import org.drools.compiler.compiler.io.memory.MemoryFileSystem
import org.kie.api.KieBase
import org.kie.api.KieServices
import org.kie.api.builder.Message.Level
import org.kie.api.io.ResourceConfiguration
import org.kie.api.io.ResourceType
import org.kie.internal.builder.DecisionTableConfiguration
import org.kie.internal.builder.DecisionTableInputType
import org.kie.internal.builder.KnowledgeBuilderFactory

object GlobalDrools_1 {
    var kbase: KieBase = null
}

class tr_classify_postsJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_classify_posts"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val Expression_0 = getExpression_0(spark)


    	val Drools_1 = getDrools_1(spark, Expression_0)


    	CSV_3(spark, Drools_1)
 
    
  }

  def getExpression_0(spark: SparkSession): Dataset[Expression_0Schema] = {
  	import spark.implicits._
    val expr = {
        Array(
            Map("id" -> new java.math.BigDecimal(1), "debit" -> "1", "credit" -> "2", "amount" -> new java.math.BigDecimal("100")),
            Map("id" -> new java.math.BigDecimal(1), "debit" -> "2", "credit" -> "3", "amount" -> new java.math.BigDecimal("200")),
            Map("id" -> new java.math.BigDecimal(2), "debit" -> "3", "credit" -> "1", "amount" -> new java.math.BigDecimal("300")) 
        )
    }
    spark.createDataset(expr.map(m => Expression_0Schema(
        id = m.getOrElse("id", null).asInstanceOf[java.math.BigDecimal],        
        debit = m.getOrElse("debit", null).asInstanceOf[java.lang.String],        
        credit = m.getOrElse("credit", null).asInstanceOf[java.lang.String],        
        amount = m.getOrElse("amount", null).asInstanceOf[java.math.BigDecimal]        
    )))
  }

  def getDrools_1(spark: SparkSession, Expression_0: Dataset[Expression_0Schema]) = {
    import spark.implicits._
    Expression_0.mapPartitions(partition => {
        if(GlobalDrools_1.kbase == null) {
            val fs = org.apache.hadoop.fs.FileSystem.get(new Configuration())
            val kieServices = KieServices.Factory.get()
            val kfs = kieServices.newKieFileSystem()
            val kieRepository = kieServices.getRepository()
        
            {
                val fileUrl = s"""/samples/classify.drl"""      
                val ins = fs.open(new Path(fileUrl))
                var buffer = IOUtils.toByteArray(ins)
                ins.close()
                val res = kieServices.getResources().newByteArrayResource(buffer)
                res.setResourceType(ResourceType.DRL)
                res.setSourcePath(s"""/samples/classify.drl""")
                kfs.write(res)
            }
            {
                val fileUrl = s"""/samples/classify.xls"""      
                val ins = fs.open(new Path(fileUrl))
                var buffer = IOUtils.toByteArray(ins)
                ins.close()
                val res = kieServices.getResources().newByteArrayResource(buffer)
                res.setResourceType(ResourceType.DTABLE)
                ;{
                    val resourceConfiguration = KnowledgeBuilderFactory.newDecisionTableConfiguration()
                    resourceConfiguration.setInputType(DecisionTableInputType.XLS)
                    res.setConfiguration(resourceConfiguration)
                }
                res.setSourcePath(s"""/samples/classify.xls""")
                kfs.write(res)
            }
            val kieBuilder = kieServices.newKieBuilder(kfs)
            kieBuilder.buildAll()
            if (kieBuilder.getResults().hasMessages(Level.ERROR)) {
                throw new RuntimeException("Build Errors:\n" + kieBuilder.getResults().toString())
            }
            val module = kieBuilder.getKieModule()
            val kieContainer = kieServices.newKieContainer(module.getReleaseId())
            val kieBaseName = kieContainer.getKieBaseNames().iterator().next()
            GlobalDrools_1.kbase = kieContainer.getKieBase(kieBaseName)
        }
        val results = new java.util.ArrayList[Drools_1Schema]()  
        partition.sliding(_slideSize, _slideSize).foreach(slide => {
            val session = GlobalDrools_1.kbase.newKieSession()
            
            
            if (_debug) {
                KieServices.Factory.get().getLoggers().newConsoleLogger(session)
            }
            val factType = GlobalDrools_1.kbase.getFactType("ru.neoflex.rules", "Record");        
            for(row <- slide){
                val obj = factType.newInstance()
                factType.set(obj, "id", row.id)
                factType.set(obj, "debit", row.debit)
                factType.set(obj, "credit", row.credit)
                factType.set(obj, "amount", row.amount)
                session.insert(obj)
            }
            session.fireAllRules()  
            val resultFactType = GlobalDrools_1.kbase.getFactType("ru.neoflex.rules", "Record")    
            val resultsQuery = session.getQueryResults("getResult")
            val resultIterator = resultsQuery.iterator()
            while(resultIterator.hasNext()){
                val r = resultIterator.next()
                val resultObject = r.get("result")
                results.add(Drools_1Schema(    
                    id = resultFactType.get(resultObject, "id").asInstanceOf[java.math.BigDecimal],    
                    debit = resultFactType.get(resultObject, "debit").asInstanceOf[java.lang.String],    
                    credit = resultFactType.get(resultObject, "credit").asInstanceOf[java.lang.String],    
                    amount = resultFactType.get(resultObject, "amount").asInstanceOf[java.math.BigDecimal],    
                    charter = resultFactType.get(resultObject, "charter").asInstanceOf[java.lang.String]            
                ))
            }
            session.dispose()          
        })       
        JavaConversions.asScalaBuffer(results).toIterator      
    })
  }

  def CSV_3(spark: SparkSession, ds: Dataset[Drools_1Schema]): Unit = {
    val path = {
      s"""/tmp/drools_out.xls"""
    }
    logger.logInfo(s"CSVTarget CSV_3 path: ${path}")

    ds.write
      .format("com.crealytics.spark.excel")
      .option("useHeader", """true""")
      .option("header", """true""")
      .mode("overwrite")
      .option("timestampFormat", """yyyy-MM-dd hh:mm:ss""")
      .option("dateFormat", """yyyy-MM-dd""")
      .save(path)
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Expression_0Schema(
    var id: java.math.BigDecimal,
    var debit: java.lang.String,
    var credit: java.lang.String,
    var amount: java.math.BigDecimal
) extends Serializable
case class Drools_1Schema(
    var id: java.math.BigDecimal,
    var debit: java.lang.String,
    var credit: java.lang.String,
    var amount: java.math.BigDecimal,
    var charter: java.lang.String
) extends Serializable

}


object tr_classify_postsJob {
   def main(args: Array[String]): Unit = {
     new tr_classify_postsJob().sparkMain(args)
  }
}

