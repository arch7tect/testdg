package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext



class tr_yandex_clickHouseJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_yandex_clickHouse"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val getLastYear = getgetLastYear(spark)


    	setLastYear(spark, getLastYear)
 
    
  }
  def getgetLastYear(spark: SparkSession) = {
  	import spark.implicits._
    
    spark
    .read 
    .format("jdbc")
    .option("user", s"""admin""")
    .option("password", s"""12tYu87b""")
    .option("ssl", s"""true""")
    .option("sslrootcert", s"""/usr/local/share/ca-certificates/Yandex/YandexInternalRootCA.crt""")
    .option("url", s"""jdbc:clickhouse://rc1a-kl9o62ybi194nx66.mdb.yandexcloud.net:8443/db1""")
    .option("dbtable", s"""lastYear""")
    .load() 
  }

  def setLastYear(spark: SparkSession, ds: Dataset[org.apache.spark.sql.Row]): Unit = {        
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Append)
        .format("jdbc")
        .option("user", s"""admin""")
        .option("password", s"""12tYu87b""")
        .option("ssl", s"""true""")
        .option("sslrootcert", s"""/usr/local/share/ca-certificates/Yandex/YandexInternalRootCA.crt""")
        .option("url", s"""jdbc:clickhouse://rc1a-kl9o62ybi194nx66.mdb.yandexcloud.net:8443/db1""")
        .option("dbtable", s"""lastYear""")
        .save()    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)

}


object tr_yandex_clickHouseJob {
   def main(args: Array[String]): Unit = {
     new tr_yandex_clickHouseJob().sparkMain(args)
  }
}

