package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext



class tr_yandex_dynamoDB_2Job extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_yandex_dynamoDB_2"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val lastYearDynamoDB = getlastYearDynamoDB(spark)


    	val SparkSQL = getSparkSQL(spark, lastYearDynamoDB)


    	setlastYearDynamoDB(spark, SparkSQL)
 
    
  }
  def getlastYearDynamoDB(spark: SparkSession) = {
  	import spark.implicits._
    import scala.reflect.runtime.universe._
    val fileName = s""""""
    val paths = fileName.split(",")
    
    spark
    .read 
    .format("dynamodb")
    .option("tableName", s"""lastYear""")
    .option("throughput", s"""1000""")
    .load() 
  }

  def getSparkSQL(spark: SparkSession, lastYearDynamoDB: Dataset[org.apache.spark.sql.Row]) = {
    import spark.implicits._
    	
    		lastYearDynamoDB.createOrReplaceTempView("UpdateTable")
    	
    			
    	val sqlText = s"""select 
    	                      personName,
    	                      businessentityid,
    	                      rate,
    	                      ratechangedate
    	                  from UpdateTable
    	                  Union All
    	                  select 
    	                      personName,
    	                      businessentityid + 20,
    	                      rate,
    	                      ratechangedate
    	                  from UpdateTable"""	
    	val queryResult = spark.sql(s"${sqlText}")
    	
    		
    	queryResult.as[SparkSQLSchema]      
  }

  def setlastYearDynamoDB(spark: SparkSession, ds: Dataset[SparkSQLSchema]): Unit = {        
    val fileName = s""""""
    logger.logInfo(s"LocalTarget setlastYearDynamoDB fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Append)
        .format("dynamodb")
        .option("tableName", s"""lastYear""")
        .option("throughput", s"""1000""")
        .save(fileName)    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class SparkSQLSchema(
    var personName: java.lang.String,
    var businessentityid: java.lang.Integer,
    var rate: java.lang.Double,
    var ratechangedate: java.lang.String
) extends Serializable

}


object tr_yandex_dynamoDB_2Job {
   def main(args: Array[String]): Unit = {
     new tr_yandex_dynamoDB_2Job().sparkMain(args)
  }
}

