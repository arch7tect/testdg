package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class tr_aw_long_time_no_saleJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_aw_long_time_no_sale"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val SalesOrderHeader = getSalesOrderHeader(spark)


    	val SQL_17 = getSQL_17(spark)


    	val Join_2 = getJoin_2(spark, SalesOrderHeader, SQL_17)


    	val Store = getStore(spark)


    	val Join_6 = getJoin_6(spark, Join_2, Store)


    	val Spark_SQL_9 = getSpark_SQL_9(spark, Join_6)


    	val Spark_SQL_13 = getSpark_SQL_13(spark, Spark_SQL_9)
    	Spark_SQL_13.persist(StorageLevel.MEMORY_ONLY)


    	Local_ORC(spark, Spark_SQL_13)

    	Local_PARQUET(spark, Spark_SQL_13)
 
    
  }
    
  def getSalesOrderHeader(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	billtoaddressid,
                      	comment,
                      	creditcardapprovalcode,
                      	creditcardid,
                      	currencyrateid,
                      	customerid,
                      	duedate,
                      	freight,
                      	modifieddate,
                      	orderdate,
                      	revisionnumber,
                      	rowguid,
                      	salesorderid,
                      	salespersonid,
                      	shipdate,
                      	shipmethodid,
                      	shiptoaddressid,
                      	status,
                      	subtotal,
                      	taxamt,
                      	territoryid,
                      	totaldue
                      from sales.salesorderheader"""
    
    logger.logInfo(s"SQLSource SalesOrderHeader query: ${sqlText}")    
    val contextName = "awSales"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[SalesOrderHeaderSchema]
  }
    
  def getSQL_17(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	customerid,
                      	modifieddate,
                      	personid,
                      	rowguid,
                      	storeid,
                      	territoryid
                      from sales.customer"""
    
    logger.logInfo(s"SQLSource SQL_17 query: ${sqlText}")    
    val contextName = "awSales"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[SQL_17Schema]
  }
    
  def getStore(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	businessentityid,
                      	demographics,
                      	modifieddate,
                      	name,
                      	rowguid,
                      	salespersonid
                      from sales.store"""
    
    logger.logInfo(s"SQLSource Store query: ${sqlText}")    
    val contextName = "awSales"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[StoreSchema]
  }

  def getJoin_2(spark: SparkSession, SalesOrderHeader: Dataset[SalesOrderHeaderSchema], SQL_17: Dataset[SQL_17Schema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    SalesOrderHeader.joinWith(SQL_17, 
        SalesOrderHeader("customerid") === SQL_17("customerid"), 
        "inner")
    .select(
    		col("_1.billtoaddressid")		.alias("billtoaddressid"),  		
    		col("_1.comment")		.alias("comment"),  		
    		col("_1.creditcardapprovalcode")		.alias("creditcardapprovalcode"),  		
    		col("_1.creditcardid")		.alias("creditcardid"),  		
    		col("_1.currencyrateid")		.alias("currencyrateid"),  		
    		col("_1.duedate")		.alias("duedate"),  		
    		col("_1.freight")		.alias("freight"),  		
    		col("_1.orderdate")		.alias("orderdate"),  		
    		col("_1.revisionnumber")		.alias("revisionnumber"),  		
    		col("_1.salesorderid")		.alias("salesorderid"),  		
    		col("_1.salespersonid")		.alias("salespersonid"),  		
    		col("_1.shipdate")		.alias("shipdate"),  		
    		col("_1.shipmethodid")		.alias("shipmethodid"),  		
    		col("_1.shiptoaddressid")		.alias("shiptoaddressid"),  		
    		col("_1.status")		.alias("status"),  		
    		col("_1.subtotal")		.alias("subtotal"),  		
    		col("_1.taxamt")		.alias("taxamt"),  		
    		col("_1.totaldue")		.alias("totaldue"),  		
    		col("_2.customerid")		.alias("customerid"),  		
    		col("_2.modifieddate")		.alias("modifieddate"),  		
    		col("_2.personid")		.alias("personid"),  		
    		col("_2.rowguid")		.alias("rowguid"),  		
    		col("_2.storeid")		.alias("storeid"),  		
    		col("_2.territoryid")		.alias("territoryid")		
    )    
    .as[Join_2Schema]
  }

  def getJoin_6(spark: SparkSession, Join_2: Dataset[Join_2Schema], Store: Dataset[StoreSchema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    Join_2.joinWith(Store, 
        Join_2("storeid") === Store("businessentityid"), 
        "inner")
    .select(
    		col("_1.billtoaddressid")		.alias("billtoaddressid"),  		
    		col("_1.comment")		.alias("comment"),  		
    		col("_1.revisionnumber")		.alias("creditcardapprovalcode"),  		
    		col("_1.salesorderid")		.alias("creditcardid"),  		
    		col("_1.salespersonid")		.alias("currencyrateid"),  		
    		col("_1.shipdate")		.alias("duedate"),  		
    		col("_1.freight")		.alias("freight"),  		
    		col("_1.orderdate")		.alias("orderdate"),  		
    		col("_1.status")		.alias("revisionnumber"),  		
    		col("_1.salesorderid")		.alias("salesorderid"),  		
    		col("_1.salespersonid")		.alias("salespersonid"),  		
    		col("_1.shipdate")		.alias("shipdate"),  		
    		col("_1.shipmethodid")		.alias("shipmethodid"),  		
    		col("_1.shiptoaddressid")		.alias("shiptoaddressid"),  		
    		col("_1.status")		.alias("status"),  		
    		col("_1.subtotal")		.alias("subtotal"),  		
    		col("_1.taxamt")		.alias("taxamt"),  		
    		col("_1.totaldue")		.alias("totaldue"),  		
    		col("_1.customerid")		.alias("customerid"),  		
    		col("_1.modifieddate")		.alias("modifieddate"),  		
    		col("_1.personid")		.alias("personid"),  		
    		col("_1.rowguid")		.alias("rowguid"),  		
    		col("_1.storeid")		.alias("storeid"),  		
    		col("_1.territoryid")		.alias("territoryid"),  		
    		col("_2.businessentityid")		.alias("businessentityid"),  		
    		col("_2.demographics")		.alias("demographics"),  		
    		col("_2.name")		.alias("name")		
    )    
    .as[Join_6Schema]
  }

  def getSpark_SQL_9(spark: SparkSession, Join_6: Dataset[Join_6Schema]) = {
    import spark.implicits._
    	
    		Join_6.createOrReplaceTempView("Sales")
    	
    			
    	val sqlText = s"""select businessentityid, customerid, storeid, name as storename, max(orderdate) as lastorderdate,
    	                  months_between(cast('${jobParameters("today")}' as date), MAX(orderdate), true) as monthssincelastorder
    	                  from Sales
    	                  group by businessentityid, customerid, storeid, name"""	
    	val queryResult = spark.sql(s"${sqlText}")
    	
    		
    	queryResult.as[Spark_SQL_9Schema]      
  }

  def getSpark_SQL_13(spark: SparkSession, Spark_SQL_9: Dataset[Spark_SQL_9Schema]) = {
    import spark.implicits._
    	
    		Spark_SQL_9.createOrReplaceTempView("Sales2")
    	
    			
    	val sqlText = s"""select * 
    	                  from Sales2
    	                  WHERE monthssincelastorder >= 12
    	                  ORDER BY monthssincelastorder DESC"""	
    	val queryResult = spark.sql(s"${sqlText}")
    	
    		
    	queryResult.as[Spark_SQL_13Schema]      
  }

  def Local_ORC(spark: SparkSession, ds: Dataset[Spark_SQL_13Schema]): Unit = {        
    val fileName = s"""/tmp/long_time_no_sale"""
    logger.logInfo(s"LocalTarget Local_ORC fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("orc")
        .save(fileName)    
    
    var partitionFields = Array[String]()
    val fieldComments = Map[String, String]()
    def makeTypeDescription(dataType: org.apache.spark.sql.types.DataType): String = {
      if (dataType.isInstanceOf[org.apache.spark.sql.types.ArrayType]) {
        "ARRAY<" + makeTypeDescription(dataType.asInstanceOf[org.apache.spark.sql.types.ArrayType].elementType) + ">"
      }
      else if (dataType.isInstanceOf[org.apache.spark.sql.types.StructType]) {
        "STRUCT<" + dataType.asInstanceOf[org.apache.spark.sql.types.StructType].toList.map(f=>f.name + ": " + makeTypeDescription(f.dataType)).mkString(", ") + ">"
      }
      else {
        dataType.typeName
      }
    }
    var fieldStr = dsOut.schema.fields.filterNot(f => partitionFields.contains(f.name.toLowerCase)).map((f)=>{f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")}).mkString(", ")    
    var partStr = dsOut.schema.fields.filter(f => partitionFields.contains(f.name.toLowerCase)).map(f => f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")).mkString(", ")
    spark.sql(s"""DROP TABLE IF EXISTS dm_long_time_no_sale""")
    val createQuery = s"""CREATE EXTERNAL TABLE dm_long_time_no_sale(${fieldStr})
    ${if(partStr == null || partStr == "") "" else s"PARTITIONED BY (${partStr})"}
    STORED AS orc
    LOCATION '/tmp/long_time_no_sale'"""
    logger.logInfo(s"Create external table:\n${createQuery}")   
    spark.sql(createQuery)
 
  }

  def Local_PARQUET(spark: SparkSession, ds: Dataset[Spark_SQL_13Schema]): Unit = {        
    val fileName = s"""/tmp/long_time_no_sale_parquet"""
    logger.logInfo(s"LocalTarget Local_PARQUET fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("parquet")
        .option("compression", s"""gzip""")
        .save(fileName)    
    
    var partitionFields = Array[String]()
    val fieldComments = Map[String, String]()
    def makeTypeDescription(dataType: org.apache.spark.sql.types.DataType): String = {
      if (dataType.isInstanceOf[org.apache.spark.sql.types.ArrayType]) {
        "ARRAY<" + makeTypeDescription(dataType.asInstanceOf[org.apache.spark.sql.types.ArrayType].elementType) + ">"
      }
      else if (dataType.isInstanceOf[org.apache.spark.sql.types.StructType]) {
        "STRUCT<" + dataType.asInstanceOf[org.apache.spark.sql.types.StructType].toList.map(f=>f.name + ": " + makeTypeDescription(f.dataType)).mkString(", ") + ">"
      }
      else {
        dataType.typeName
      }
    }
    var fieldStr = dsOut.schema.fields.filterNot(f => partitionFields.contains(f.name.toLowerCase)).map((f)=>{f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")}).mkString(", ")    
    var partStr = dsOut.schema.fields.filter(f => partitionFields.contains(f.name.toLowerCase)).map(f => f.name.toLowerCase + " " + makeTypeDescription(f.dataType) + " " + (if (fieldComments.getOrElse(f.name, null) != null) " COMMENT " + "\"" + fieldComments(f.name) + "\"" else "")).mkString(", ")
    spark.sql(s"""DROP TABLE IF EXISTS dm_long_time_no_sale_parquet""")
    val createQuery = s"""CREATE EXTERNAL TABLE dm_long_time_no_sale_parquet(${fieldStr})
    OPTIONS (
        'compression'='gzip' 
    )
    ${if(partStr == null || partStr == "") "" else s"PARTITIONED BY (${partStr})"}
    STORED AS parquet
    LOCATION '/tmp/long_time_no_sale_parquet'"""
    logger.logInfo(s"Create external table:\n${createQuery}")   
    spark.sql(createQuery)
 
  }


    override def initBuilder(builder: SparkSession.Builder): SparkSession.Builder = {
        builder.enableHiveSupport()
    }
     

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class SalesOrderHeaderSchema(
    var billtoaddressid: java.lang.Integer,
    var comment: java.lang.String,
    var creditcardapprovalcode: java.lang.String,
    var creditcardid: java.lang.Integer,
    var currencyrateid: java.lang.Integer,
    var customerid: java.lang.Integer,
    var duedate: java.sql.Timestamp,
    var freight: java.math.BigDecimal,
    var modifieddate: java.sql.Timestamp,
    var orderdate: java.sql.Timestamp,
    var revisionnumber: java.lang.String,
    var rowguid: java.lang.String,
    var salesorderid: java.lang.Integer,
    var salespersonid: java.lang.Integer,
    var shipdate: java.sql.Timestamp,
    var shipmethodid: java.lang.Integer,
    var shiptoaddressid: java.lang.Integer,
    var status: java.lang.String,
    var subtotal: java.math.BigDecimal,
    var taxamt: java.math.BigDecimal,
    var territoryid: java.lang.Integer,
    var totaldue: java.math.BigDecimal
) extends Serializable
case class SQL_17Schema(
    var customerid: java.lang.Integer,
    var modifieddate: java.sql.Timestamp,
    var personid: java.lang.Integer,
    var rowguid: java.lang.String,
    var storeid: java.lang.Integer,
    var territoryid: java.lang.Integer
) extends Serializable
case class StoreSchema(
    var businessentityid: java.lang.Integer,
    var demographics: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var name: java.lang.String,
    var rowguid: java.lang.String,
    var salespersonid: java.lang.Integer
) extends Serializable
case class Join_2Schema(
    var billtoaddressid: java.lang.Integer,
    var comment: java.lang.String,
    var creditcardapprovalcode: java.lang.String,
    var creditcardid: java.lang.Integer,
    var currencyrateid: java.lang.Integer,
    var duedate: java.sql.Timestamp,
    var freight: java.math.BigDecimal,
    var orderdate: java.sql.Timestamp,
    var revisionnumber: java.lang.String,
    var salesorderid: java.lang.Integer,
    var salespersonid: java.lang.Integer,
    var shipdate: java.sql.Timestamp,
    var shipmethodid: java.lang.Integer,
    var shiptoaddressid: java.lang.Integer,
    var status: java.lang.String,
    var subtotal: java.math.BigDecimal,
    var taxamt: java.math.BigDecimal,
    var totaldue: java.math.BigDecimal,
    var customerid: java.lang.Integer,
    var modifieddate: java.sql.Timestamp,
    var personid: java.lang.Integer,
    var rowguid: java.lang.String,
    var storeid: java.lang.Integer,
    var territoryid: java.lang.Integer
) extends Serializable
case class Join_6Schema(
    var billtoaddressid: java.lang.Integer,
    var comment: java.lang.String,
    var creditcardapprovalcode: java.lang.String,
    var creditcardid: java.lang.Integer,
    var currencyrateid: java.lang.Integer,
    var duedate: java.sql.Timestamp,
    var freight: java.math.BigDecimal,
    var orderdate: java.sql.Timestamp,
    var revisionnumber: java.lang.String,
    var salesorderid: java.lang.Integer,
    var salespersonid: java.lang.Integer,
    var shipdate: java.sql.Timestamp,
    var shipmethodid: java.lang.Integer,
    var shiptoaddressid: java.lang.Integer,
    var status: java.lang.String,
    var subtotal: java.math.BigDecimal,
    var taxamt: java.math.BigDecimal,
    var totaldue: java.math.BigDecimal,
    var customerid: java.lang.Integer,
    var modifieddate: java.sql.Timestamp,
    var personid: java.lang.Integer,
    var rowguid: java.lang.String,
    var storeid: java.lang.Integer,
    var territoryid: java.lang.Integer,
    var businessentityid: java.lang.Integer,
    var demographics: java.lang.String,
    var name: java.lang.String
) extends Serializable
case class Spark_SQL_9Schema(
    var businessentityid: java.lang.Integer,
    var customerid: java.lang.Integer,
    var storeid: java.lang.Integer,
    var storename: java.lang.String,
    var lastorderdate: java.sql.Timestamp,
    var monthssincelastorder: java.lang.Double
) extends Serializable
case class Spark_SQL_13Schema(
    var businessentityid: java.lang.Integer,
    var customerid: java.lang.Integer,
    var storeid: java.lang.Integer,
    var storename: java.lang.String,
    var lastorderdate: java.sql.Timestamp,
    var monthssincelastorder: java.lang.Double
) extends Serializable

}


object tr_aw_long_time_no_saleJob {
   def main(args: Array[String]): Unit = {
     new tr_aw_long_time_no_saleJob().sparkMain(args)
  }
}

