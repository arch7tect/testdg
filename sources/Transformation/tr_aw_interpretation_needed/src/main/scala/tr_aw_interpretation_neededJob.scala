package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class tr_aw_interpretation_neededJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_aw_interpretation_needed"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val productdescription = getproductdescription(spark)


    	val productmodelproductdescriptionculture = getproductmodelproductdescriptionculture(spark)


    	val Join_4 = getJoin_4(spark, productdescription, productmodelproductdescriptionculture)


    	val productmodel = getproductmodel(spark)


    	val Join_7 = getJoin_7(spark, Join_4, productmodel)


    	val culture = getculture(spark)


    	val Join_10 = getJoin_10(spark, Join_7, culture)


    	val Selection_14 = getSelection_14(spark, Join_10)


    	Table_13(spark, Selection_14)
 
    
  }
    
  def getproductdescription(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	description,
                      	modifieddate,
                      	productdescriptionid,
                      	rowguid
                      from production.productdescription"""
    
    logger.logInfo(s"SQLSource productdescription query: ${sqlText}")    
    val contextName = "awProduction"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[productdescriptionSchema]
  }
    
  def getproductmodelproductdescriptionculture(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	cultureid,
                      	modifieddate,
                      	productdescriptionid,
                      	productmodelid
                      from production.productmodelproductdescriptionculture"""
    
    logger.logInfo(s"SQLSource productmodelproductdescriptionculture query: ${sqlText}")    
    val contextName = "awProduction"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[productmodelproductdescriptioncultureSchema]
  }
    
  def getproductmodel(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	catalogdescription,
                      	instructions,
                      	modifieddate,
                      	name,
                      	productmodelid,
                      	rowguid
                      from production.productmodel"""
    
    logger.logInfo(s"SQLSource productmodel query: ${sqlText}")    
    val contextName = "awProduction"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[productmodelSchema]
  }
    
  def getculture(spark: SparkSession) = {
  
    import spark.implicits._
  
    val sqlText = s"""select
                      	cultureid,
                      	modifieddate,
                      	name
                      from production.culture"""
    
    logger.logInfo(s"SQLSource culture query: ${sqlText}")    
    val contextName = "awProduction"
    val context: JdbcETLContext = getContext(contextName).asInstanceOf[JdbcETLContext]
    
    
        
    val ds = spark.read.format("jdbc").options(Map(
               "url" -> context._url,
               "dbtable" -> ("(" + sqlText + ") t"),
               "driver" -> context._driverClassName,
               "user" -> context._user,
               "password" -> context._password,
               "fetchSize" -> _fetchSize.toString
            ))		    
    ds.load()    .as[cultureSchema]
  }

  def getJoin_4(spark: SparkSession, productdescription: Dataset[productdescriptionSchema], productmodelproductdescriptionculture: Dataset[productmodelproductdescriptioncultureSchema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    productdescription.joinWith(productmodelproductdescriptionculture, 
        productdescription("productdescriptionid") === productmodelproductdescriptionculture("productdescriptionid"), 
        "inner")
    .select(
    		col("_1.description")		.alias("description"),  		
    		col("_1.modifieddate")		.alias("modifieddate"),  		
    		col("_1.productdescriptionid")		.alias("productdescriptionid"),  		
    		col("_1.rowguid")		.alias("rowguid"),  		
    		col("_2.cultureid")		.alias("cultureid"),  		
    		col("_2.productmodelid")		.alias("productmodelid")		
    )    
    .as[Join_4Schema]
  }

  def getJoin_7(spark: SparkSession, Join_4: Dataset[Join_4Schema], productmodel: Dataset[productmodelSchema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    Join_4.joinWith(productmodel, 
        Join_4("productmodelid") === productmodel("productmodelid"), 
        "inner")
    .select(
    		col("_1.description")		.alias("description"),  		
    		col("_1.modifieddate")		.alias("modifieddate"),  		
    		col("_1.productdescriptionid")		.alias("productdescriptionid"),  		
    		col("_1.rowguid")		.alias("rowguid"),  		
    		col("_1.cultureid")		.alias("cultureid"),  		
    		col("_1.productmodelid")		.alias("productmodelid"),  		
    		col("_2.catalogdescription")		.alias("catalogdescription"),  		
    		col("_2.instructions")		.alias("instructions"),  		
    		col("_2.name")		.alias("productmodel")		
    )    
    .as[Join_7Schema]
  }

  def getJoin_10(spark: SparkSession, Join_7: Dataset[Join_7Schema], culture: Dataset[cultureSchema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    Join_7.joinWith(culture, 
        Join_7("cultureid") === culture("cultureid"), 
        "inner")
    .select(
    		col("_1.productmodelid")		.alias("productmodelid"),  		
    		col("_1.productmodel")		.alias("productmodel"),  		
    		col("_2.name")		.alias("language"),  		
    		col("_1.description")		.alias("description")		
    )    
    .as[Join_10Schema]
  }

  def getSelection_14(spark: SparkSession, Join_10: Dataset[Join_10Schema]) = {
    import spark.implicits._
       
    Join_10.filter(s"""language <> 'English'""")
  }
  
  def Table_13(spark: SparkSession, ds: Dataset[Join_10Schema]): Unit = {
  
    import spark.implicits._
    import scala.util.parsing.json.JSONObject
    
    val master: String = _master
    val jobStartTime: Long = spark.sparkContext.startTime
    val context = getContext("dwh").asInstanceOf[JdbcETLContext]
    
    notifyStart("Table_13", "Selection_14", jobStartTime)
    executeUpdate(context, s"""delete from ${context._schema}."interpretation_needed"""")
    
    
    
      ds.mapPartitions(partition => {
      val rejects = new java.util.ArrayList[(Join_10Schema, String)]()
      val cn = context.getConnection
      
      try {
          cn.setAutoCommit(false)
          val sqlText = "insert into " + context._schema + s"""."interpretation_needed"("productmodelid", "productmodel", "language", "description") 
          values( ?,  ?,  ?,  ?)"""
 
          
          logger.logInfo(s"TableTarget Table_13 query: ${sqlText}")
          
          val stmt = cn.prepareCall(sqlText)
          
          try {
            partition.sliding(_slideSize, _slideSize).foreach(slide => {
              processSlice(cn, stmt, slide.toList, (row: Join_10Schema, stmt: CallableStatement) =>{
                stmt.setObject(1, row.productmodelid.asInstanceOf[java.lang.Integer])
                stmt.setString(2, row.productmodel.asInstanceOf[java.lang.String])
                stmt.setString(3, row.language.asInstanceOf[java.lang.String])
                stmt.setString(4, row.description.asInstanceOf[java.lang.String])
              },(successRowCount:Int) =>{
                notifyExecUpdate("Table_13", "Selection_14", jobStartTime, successRowCount)
              },(errorMessage: String, failedRow: Join_10Schema) =>{
                if (rejects.size >= _rejectSize) {
                  notifyException("Table_13", "Selection_14", jobStartTime, errorMessage, failedRow)
                  throw new RuntimeException(s"Max Rejects Limit (${_rejectSize}) Was Reached for Table_13" + errorMessage)
                }
                notifyException("Table_13", "Selection_14", jobStartTime, errorMessage, failedRow)
                rejects.add((failedRow, errorMessage))
              })
            })
          } finally {
            stmt.close()
          }
      } finally {
          cn.close()
      }
      JavaConversions.asScalaBuffer(rejects).toIterator
    })
    .map { reject =>{
            val row = reject._1
            val rowMap = row.getClass.getDeclaredFields.map( _.getName ).zip( row.productIterator.to.map(v=>v match {case null=> "<null>" case _=> v}) ).toMap    
                        
            val rejectJson = JSONObject(rowMap).toString()
            (_workflowId, _applicationId, getApplicationName, "Table_13", rejectJson, reject._2, System.currentTimeMillis)
        }
    }
    .toDF("workflowid", "appid", "classname", "methodname", "object", "exception", "ts")
    .coalesce(1).write.mode(SaveMode.Append).format("json").save(s"${_applicationHome}/rejects.json")

    notifyFinish("Table_13", "Selection_14", jobStartTime)
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class productdescriptionSchema(
    var description: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var productdescriptionid: java.lang.Integer,
    var rowguid: java.lang.String
) extends Serializable
case class productmodelproductdescriptioncultureSchema(
    var cultureid: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var productdescriptionid: java.lang.Integer,
    var productmodelid: java.lang.Integer
) extends Serializable
case class productmodelSchema(
    var catalogdescription: java.lang.String,
    var instructions: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var name: java.lang.String,
    var productmodelid: java.lang.Integer,
    var rowguid: java.lang.String
) extends Serializable
case class cultureSchema(
    var cultureid: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var name: java.lang.String
) extends Serializable
case class Join_4Schema(
    var description: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var productdescriptionid: java.lang.Integer,
    var rowguid: java.lang.String,
    var cultureid: java.lang.String,
    var productmodelid: java.lang.Integer
) extends Serializable
case class Join_7Schema(
    var description: java.lang.String,
    var modifieddate: java.sql.Timestamp,
    var productdescriptionid: java.lang.Integer,
    var rowguid: java.lang.String,
    var cultureid: java.lang.String,
    var productmodelid: java.lang.Integer,
    var catalogdescription: java.lang.String,
    var instructions: java.lang.String,
    var productmodel: java.lang.String
) extends Serializable
case class Join_10Schema(
    var productmodelid: java.lang.Integer,
    var productmodel: java.lang.String,
    var language: java.lang.String,
    var description: java.lang.String
) extends Serializable


}


object tr_aw_interpretation_neededJob {
   def main(args: Array[String]): Unit = {
     new tr_aw_interpretation_neededJob().sparkMain(args)
  }
}

