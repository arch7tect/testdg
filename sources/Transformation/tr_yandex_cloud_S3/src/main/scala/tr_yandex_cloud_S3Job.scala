package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext

import org.apache.spark.sql.types.NullType


class tr_yandex_cloud_S3Job extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "tr_yandex_cloud_S3"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val employeepayhistory = getemployeepayhistory(spark)


    	val lastYear = getlastYear(spark, employeepayhistory)


    	val person = getperson(spark)


    	val joinSalary = getjoinSalary(spark, lastYear, person)
    	joinSalary.persist(StorageLevel.MEMORY_ONLY)


    	S3lastYearORC(spark, joinSalary)

    	S3lastYearCSV(spark, joinSalary)
 
    
  }

  def getemployeepayhistory(spark: SparkSession) = {
  	import spark.implicits._
   	import scala.reflect.runtime.universe._
    val schema = newProductEncoder(typeTag[employeepayhistorySchema]).schema
    val path = {
      s"""s3a://datagram/forTransformation/employeepayhistory.csv"""
    }
    
    try {
      spark.read
.schema(schema)
      .option("header", """true""") 
      .option("charset", """UTF-8""")
      .option("sep", """,""") 
      .option("quote", """"""") 
      .option("escape", """\""")
 
      .option("comment", """#""") 
  
      .csv(path)
 
.as[employeepayhistorySchema]      
	  } catch {
          case e: UnsupportedOperationException =>  spark.emptyDataset[employeepayhistorySchema]      }
    
  }

  def getperson(spark: SparkSession) = {
  	import spark.implicits._
   	import scala.reflect.runtime.universe._
    val schema = newProductEncoder(typeTag[personSchema]).schema
    val path = {
      s"""s3a://datagram/forTransformation/person.csv"""
    }
    
    try {
      spark.read
.schema(schema)
      .option("header", """true""") 
      .option("charset", """UTF-8""")
      .option("sep", """,""") 
      .option("quote", """"""") 
      .option("escape", """\""")
 
      .option("comment", """#""") 
  
      .csv(path)
 
.as[personSchema]      
	  } catch {
          case e: UnsupportedOperationException =>  spark.emptyDataset[personSchema]      }
    
  }

  def getlastYear(spark: SparkSession, employeepayhistory: Dataset[employeepayhistorySchema]) = {
    import spark.implicits._
    	
    		employeepayhistory.createOrReplaceTempView("Salary")
    	
    			
    	val sqlText = s"""select 
    	                      businessentityid,
    	                      rate,
    	                      ratechangedate
    	                  from Salary
    	                  where months_between(cast('${jobParameters("currentDate").asInstanceOf[String]}' as date), ratechangedate, true) <= 12"""	
    	val queryResult = spark.sql(s"${sqlText}")
    	
    		
    	queryResult.as[lastYearSchema]      
  }

  def getjoinSalary(spark: SparkSession, lastYear: Dataset[lastYearSchema], person: Dataset[personSchema]) = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    lastYear.joinWith(person, 
        lastYear("businessentityid") === person("businessentityid"), 
        "left_outer")
    .select(
    		col("_1.businessentityid")		.alias("businessentityid"),  		
     expr(s"""concat( 
                  if(_2.firstname is null, '', _2.firstname), 
                  ' ', 
                  if(_2.middlename is null, '', _2.middlename), 
                  ' ',
                  if(_2.lastname is null, '', _2.lastname) 
              )""")		.alias("personName"),  		
    		col("_1.rate")		.alias("rate"),  		
    		col("_1.ratechangedate")		.alias("ratechangedate")		
    )    
    .as[joinSalarySchema]
  }

  def S3lastYearORC(spark: SparkSession, ds: Dataset[joinSalarySchema]): Unit = {        
    val fileName = s"""s3a://datagram/forTransformation/lastYearORC"""
    logger.logInfo(s"LocalTarget S3lastYearORC fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Overwrite)
        .format("orc")
        .save(fileName)    
    
 
  }

  def S3lastYearCSV(spark: SparkSession, ds: Dataset[joinSalarySchema]): Unit = {
    val path = {
      s"""s3a://datagram/forTransformation/lastYearCSV"""
    }
    logger.logInfo(s"CSVTarget S3lastYearCSV path: ${path}")

    ds.write
      .mode("overwrite")
      .option("header", """true""")
      .option("charset", """UTF-8""")
      .option("sep", """,""")
      .option("quote", """"""")
      .option("escape", """\""")
      .option("comment", """#""")
      .option("timestampFormat", """yyyy-MM-dd""")
      .csv(path)
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class employeepayhistorySchema(
    var businessentityid: java.lang.Integer,
    var rate: java.lang.Double,
    var ratechangedate: java.lang.String
) extends Serializable
case class personSchema(
    var businessentityid: java.lang.Integer,
    var firstname: java.lang.String,
    var lastname: java.lang.String,
    var middlename: java.lang.String
) extends Serializable
case class lastYearSchema(
    var businessentityid: java.lang.Integer,
    var rate: java.lang.Double,
    var ratechangedate: java.lang.String
) extends Serializable
case class joinSalarySchema(
    var businessentityid: java.lang.Integer,
    var personName: java.lang.String,
    var rate: java.lang.Double,
    var ratechangedate: java.lang.String
) extends Serializable

}


object tr_yandex_cloud_S3Job {
   def main(args: Array[String]): Unit = {
     new tr_yandex_cloud_S3Job().sparkMain(args)
  }
}

